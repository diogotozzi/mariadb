# pip3 install mysql-connector-python
import mysql.connector
import random
import string

def random_string(string_length = 100):
    # string_length = random.randrange(20, 100)
    letters = string.ascii_letters + string.digits + " "
    return ''.join(random.choice(letters) for i in range(string_length))

def main():
    database = mysql.connector.connect(
      host="localhost",
      user="root",
      passwd="root",
      database="temporal_database"
    )

    cursor = database.cursor()

    sql1 = "INSERT INTO customer (name, address, telephone, document, city) VALUES (%s, %s, %s, %s, %s)"
    sql2 = "INSERT INTO customer_versioning (name, address, telephone, document, city) VALUES (%s, %s, %s, %s, %s)"
    val = []

    for x in range(100):
        for y in range(100):
            val.append((random_string(), random_string(), random_string(), random_string(), random_string()))
        cursor.executemany(sql1, val)
        cursor.executemany(sql2, val)
        database.commit()
        print("Commiting into db... " + str(x))

    print(cursor.rowcount, "rows was inserted.")

main()
