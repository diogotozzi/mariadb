DROP DATABASE IF EXISTS temporal_database;
CREATE DATABASE temporal_database CHARACTER SET = 'utf8';
USE temporal_database;

CREATE TABLE customer (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    address VARCHAR(255),
    telephone VARCHAR(255),
    document VARCHAR(255),
    city VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE customer_versioning (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    address VARCHAR(255),
    telephone VARCHAR(255),
    document VARCHAR(255),
    city VARCHAR(255),
    PRIMARY KEY (id)
) WITH SYSTEM VERSIONING;

-- UPDATE customer SET city = "Sao Paulo" WHERE id IS NOT NULL;
-- UPDATE customer_versioning SET city = "Sao Paulo" WHERE id IS NOT NULL;

-- SELECT * FROM customer_versioning FOR SYSTEM_TIME ALL;
-- SELECT * FROM customer_versioning FOR SYSTEM_TIME AS OF TIMESTAMP '2020-03-31 08:07:06';
